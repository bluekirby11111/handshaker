use thiserror::Error;

use crate::levin;

#[derive(Error, Debug)]
pub enum Error {
    #[error("MessageError: {0}")]
    MessageError(#[from] levin::MessageError),
    #[error("std::io::Error: {0}")]
    IoError(#[from] std::io::Error),
}
