// Messages in Levin protocol can be found here:
// https://github.com/monero-project/monero/blob/master/docs/LEVIN_PROTOCOL.md
// The constants were based in this document
// https://github.com/monero-project/monero/blob/8123d945f874548e87d3850b6633f047120ece45/contrib/epee/include/net/levin_base.h

use std::array::TryFromSliceError;

use thiserror::Error;

const LEVIN_SIGNATURE: u64 = 0x0101010101012101; //Bender's nightmare

pub const LEVIN_PACKET_REQUEST: u32 = 0x00000001;
//pub const LEVIN_PACKET_RESPONSE: u32 = 0x00000002;
//pub const LEVIN_PACKET_BEGIN: u32 = 0x00000004;
//pub const LEVIN_PACKET_END: u32 = 0x00000008;

const LEVIN_PROTOCOL_VER_1: u32 = 1;

#[derive(Error, Debug)]
pub enum MessageError {
    #[error("TryFromSliceError: {0}")]
    TryFromSliceError(#[from] TryFromSliceError),
    #[error("Invalid Length: {0}")]
    InvalidLength(usize),
}

#[derive(Debug, Clone)]
pub struct Message {
    pub signature: u64,
    pub cb: u64,
    pub have_to_return_data: u8,
    pub command: u32,
    pub return_code: i32,
    pub flags: u32,
    pub protocol_version: u32,
}

impl Message {
    pub fn make_header(
        command: u32,
        msg_size: u64,
        flags: u32,
        return_code: i32,
        expect_response: bool,
    ) -> Self {
        Self {
            signature: LEVIN_SIGNATURE,
            cb: msg_size,
            have_to_return_data: expect_response as u8,
            command,
            return_code,
            flags,
            protocol_version: LEVIN_PROTOCOL_VER_1,
        }
    }

    pub fn to_bytes(self) -> Vec<u8> {
        <Message as Into<Vec<u8>>>::into(self)
    }
}

impl Into<Vec<u8>> for Message {
    fn into(self) -> Vec<u8> {
        const SIZE: usize = std::mem::size_of::<Message>();
        let mut buffer = Vec::with_capacity(SIZE);

        buffer.extend(self.signature.to_le_bytes());
        buffer.extend(self.cb.to_le_bytes());
        buffer.extend(self.have_to_return_data.to_le_bytes());
        buffer.extend(self.command.to_le_bytes());
        buffer.extend(self.return_code.to_le_bytes());
        buffer.extend(self.flags.to_le_bytes());
        buffer.extend(self.protocol_version.to_le_bytes());

        buffer
    }
}

impl TryFrom<Vec<u8>> for Message {
    type Error = MessageError;

    fn try_from(value: Vec<u8>) -> Result<Self, Self::Error> {
        if value.len() < 33 {
            return Err(MessageError::InvalidLength(value.len()));
        }
        let signature = u64::from_le_bytes(value[0..8].try_into()?);
        let msg_size = u64::from_le_bytes(value[8..16].try_into()?);

        let e_response = value[16];
        let command = u32::from_le_bytes(value[17..21].try_into()?);
        let return_code = i32::from_le_bytes(value[21..25].try_into()?);
        let flags = u32::from_le_bytes(value[25..29].try_into()?);
        let version = u32::from_le_bytes(value[29..33].try_into()?);

        Ok(Self {
            signature,
            cb: msg_size,
            have_to_return_data: e_response,
            command,
            return_code,
            flags,
            protocol_version: version,
        })
    }
}
