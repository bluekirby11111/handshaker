mod error;
mod levin;

use std::{
    io::{Read, Write},
    net::{IpAddr, SocketAddr, TcpStream},
};

use clap::Parser;
use error::Error;

use crate::levin::{Message, LEVIN_PACKET_REQUEST};
#[derive(clap::Parser, Debug)]
struct CliArgs {
    #[clap(
        long = "address",
        short = 'a',
        env = "ADDRESS",
        default_value = "127.0.0.1"
    )]
    address: IpAddr,
    #[clap(long = "port", short = 'p', env = "PORT", default_value = "18080")]
    port: u16,
}

fn main() -> Result<(), Error> {
    dotenv::dotenv().ok();

    let CliArgs { address, port } = CliArgs::parse();

    let addr = SocketAddr::new(address, port);
    let mut stream = TcpStream::connect(addr)?;
    println!("{:?}", &stream);

    let message = Message::make_header(1001, 0, LEVIN_PACKET_REQUEST, 0, true);
    println!("{:?}", &message);

    stream.write_all(&message.to_bytes())?;
    stream.flush()?;

    let mut buf = Vec::<u8>::new();
    stream.read_to_end(&mut buf)?;
    let response = Message::try_from(buf)?;
    println!("{:?}", &response);

    Ok(())
}
