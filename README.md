# xmr-handshake
Perform a levin protocol handshake using TCP. The protocol definitions can be found [here](https://github.com/monero-project/monero/blob/master/docs/LEVIN_PROTOCOL.md). This implementation was based on the [monero node implementation](https://github.com/monero-project/monero/blob/8123d945f874548e87d3850b6633f047120ece45/contrib/epee/include/net/levin_base.h).

## Building
You will need rust 1.66 or later. 

```bash
$ cargo build --release
```

## Executing
You will need to run a [monero node](https://www.monero.how/how-to-run-monero-node).

After running the monero node, run:
```bash
$ cargo run -- -- address <ADDRESS> --port <PORT>
```

The default address is `127.0.0.1` and default port is `18080`

If the handshake is successful, you should receive the following message:
```json
Message { signature: 72340172838084865, cb: 0, have_to_return_data: 0, command: 1001, return_code: -1, flags: 2, protocol_version: 1 }
```

